import os
import subprocess
import csv

ip_container = '172.19.0.2'
user = 'postgres'
database = 'postgres'
path = '../benchmark/'

def find_times(output):
    output_splitted = output.splitlines()
    planning_time_in_ms = ''
    execution_time_in_ms = ''
    for line in output_splitted:
        if line.startswith(' Planning Time'):
            planning_time_in_ms = line[16:len(line) - 3]
        if line.startswith(' Execution Time'):
            execution_time_in_ms = line[17:len(line) - 3]
    return planning_time_in_ms, execution_time_in_ms


def find_error(error):
    error_splitted = error.splitlines()
    for line in error_splitted:
        if line.startswith('ERROR:'):
            return line[8:]
    return ''


with open('output.csv', mode='w') as result_file:
    output_writer = csv.writer(result_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    output_writer.writerow(['Query', 'error', 'planned [ms]', 'executed [ms]', 'outputDexter', 'errorDexter'])

    for filename in os.listdir(path):
        if filename.startswith('fkindexes') or filename.startswith('schema'):
            continue

        if not filename.endswith('.sql') or not filename.startswith('7b'):
            continue

        query = open(path + filename).read()
        analyze_query = 'EXPLAIN ANALYZE ' + query

        # query = "EXPLAIN ANALYZE SELECT * FROM title, movie_companies WHERE title.id = movie_companies.movie_id"
        # query = "SELECT COUNT(*) FROM role_type"

        # print(filename)

        execute_psql = f"psql -h {ip_container} -U {user} {database} -c '\\timing' -c \"{analyze_query}\""
        runs = 6
        planned_total = 0
        executed_total = 0
        for i in range(runs):
            result_psql = subprocess.run(execute_psql, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if i is 0:
                error_psql = result_psql.stderr.decode('utf-8')
                error_psql = find_error(error_psql)
            else:
                output_psql = result_psql.stdout.decode('utf-8')
                planned, executed = find_times(output_psql)
                planned_total = planned_total + float(planned)
                executed_total = executed_total + float(executed)
        planned_total = planned_total / (runs - 1)
        executed_total = executed_total / (runs - 1)

        create = '--create' if False else ''

        execute_dexter = f"dexter -h {ip_container} -U {user} {database} {create} -s \"{query}\""
        result_dexter = subprocess.run(execute_dexter, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output_dexter = result_dexter.stdout.decode('utf-8')
        error_dexter = result_dexter.stderr.decode('utf-8')

        print(filename, error_psql, "{:.3f}".format(planned_total), "{:.3f}".format(executed_total), output_dexter)
        output_writer.writerow([filename, error_psql, "{:.3f}".format(planned_total), "{:.3f}".format(executed_total),
                                output_dexter.rstrip(), error_dexter.rstrip()])
        # break
