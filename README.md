# dexter-pg

Automated Database Index Optimization for PostgreSQL

## Overview Docker Container

| **container** | postgres-hypopg | join-order-benchmark / load-benchmark-hypopg | python-example | dexter |
| ------ | ------ | ------ | ------ | ------ |
| **base-image** | postgres:11 | python:3 | python:3 | ruby:2.6 |
| **explanation** | standard & improved data | can import data to postgres-hypopg | python script to run queries with psql | empty |
| **data** | docker-entrypoint-initdb.d/000_setup.sql & docker-entrypoint.sh | data/*.list.gz & setup_db.sh (import *.gz files into db with imdbpy)| join-order-bechmark/*.sql & run_sql.py | ------ |
| **programs** | psql 11.5 & postgres 11.5 | dockerize (contains pip (imdbpy/lxml/psycopg2-binary/setuptools/SQLAlchemy/wheel) /python/imdbpy/...) | postgresql-client & dockerize (psycopg2-binary) | dockerize & dexter |

## Good to know
* Docker is a set of platform-as-a-service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.
* Docker-Compose is a tool for defining and running multi-container Docker applications.
* gem is a package manager for ruby
* pip is a package manager for pyhton
* dockerize is a utility to simplify running applications in docker containers
* imdb is a move database, [https://www.imdb.com/]
* IMDbPY is a Python package useful to retrieve and manage the data of the IMDb movie database
* Psycopg is a PostgreSQL database adapter for the Python programming language

## Commands
* list all images\
`sudo docker images`

* list all containers\
`sudo docker ps`

* retag an image (rename)\
`sudo docker tag <c8f1d7701e68> <dexter-pg_new-name>`\
`sudo docker rmi <dexter-pg_old-name>`

* build one container\
`sudo docker build --pull <image-name>`

* start container postgres-hypopg\
`sudo docker-compose up -d postgres-hypopg`

* stop one container\
`sudo docker-compose stop postgres-hypopg`

* open connection in postgres-hypopg container to data\
`sudo docker-compose run --rm postgres-hypopg bash -c "psql postgres://postgres:postgres@postgres-hypopg/postgres"`

* start bash in dexter container\
`sudo docker-compose run --rm dexter bash`\

* start python example\
`sudo docker-compose run --rm python-example`

* get ip address of container\
`sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dexter-pg_postgres-hypopg_1`

* run sql command from host\
`psql -h 172.19.0.2 -U postgres postgres -c '\timing' -c "EXPLAIN ANALYZE SELECT * FROM title LIMIT 100"`


## How to install docker-compose
from https://github.com/docker/compose/releases: \
``curl -L https://github.com/docker/compose/releases/download/1.24.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose`` \
`chmod +x /usr/local/bin/docker-compose`

## How to install docker
from https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04 \
``curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`` \
``sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`` \
``sudo apt-get update`` \
``apt-cache policy docker-ce`` \
And finally, \
``sudo apt-get install -y docker-ce`` \
Docker's status can be checked using: \
``sudo systemctl status docker``
